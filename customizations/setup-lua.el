;;; -*- lexical-binding: t; -*-


(use-package lua-mode
  :ensure t
  :config
  ;; (setq lsp-clients-luarocks-bin-dir "/usr/local/bin/")
  ;; (setq lsp-clients-lua-lsp-server-install-dir "/usr/local/bin/lua-lsp")
  (setq lsp-clients-lua-language-server-bin "~/.emacs.d/.cache/lsp/lua-language-server/bin/macOS/lua-language-server")
  ;; (setq lsp-lua-diagnostics-globals t)
  (setq lsp-lua-diagnostics-disable (vector "unused-local" "lowercase-global" "trailing-space" "empty-block"))
  (setq lsp-lua-diagnostics-globals (vector "global" "flags"))
  ;; (setq lsp-lua-diagnostics-disable nil)
  :hook ((lua-mode . lsp))
  )
